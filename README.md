# AndrOCR

A simple text recognition application that preforms optical character
recognition (OCR). on images and translates the resulting text to different
languages. [Tesseract](https://code.google.com/p/tesseract-ocr/) OCR library
is used for character recognition and Microsoft Translate API for translations.

## Requirements
- [tess-two](https://github.com/rmtheis/tess-two) built as a library project
- Windows Azure Marketplace Client ID and Client Secret for translation
package com.justus.androcr;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

/**
 * Initializes tesseract and performs an asynchronous chharcater recognition
 * on a bitmap
 * 
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class OcrTask extends AsyncTask<Bitmap, Void, OcrResult> {
  private static final String TAG = OcrTask.class.getSimpleName();

  private TessBaseAPI mTess;
  private OcrCallback mCallback;
  private Uri mBitmapUri;

  OcrTask(TessBaseAPI ocr, OcrCallback callback, Uri bitmapUri) {
    mTess = ocr;
    mCallback = callback;
    mBitmapUri = bitmapUri;
  }

  /**
   * Perform asynchronous text recognition on given bitmap
   * @param bitmap
   * @return the OCR result object
   */
  protected OcrResult doInBackground(Bitmap... bitmap) {
    long start = System.currentTimeMillis();
    String resultText = null;
    try {
      mTess.setImage(bitmap[0]);
      resultText = mTess.getUTF8Text();
    } catch (RuntimeException e) {
      Log.e(TAG, "OCR failed: " + e.getMessage());
      e.printStackTrace();
    }
    resultText = resultText.replaceAll("[^a-zA-ZäöüÄÖÜ0-9]+", " ").trim();
    long elapsed = System.currentTimeMillis() - start;
    OcrResult result = new OcrResult(mBitmapUri, resultText, elapsed,
        mTess.wordConfidences(), mTess.meanConfidence());
    mTess.end();
    return result;
  }
  
  @Override
  protected void onPostExecute(OcrResult result) {
    mCallback.onOcrComplete(result);
  }
}

package com.justus.androcr;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a result of an OCR scan
 * 
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class OcrResult implements Parcelable {
  
  /**
   * Uri pointing to the inspected bitmap. Only the uri is stored in order to
   * avoid hitting the memory limit while parceling the result between activities
   */
  private Uri mBitmapUri;
  
  /** Recognized text */
  private String mText;
  
  /** Time elapsed */
  private long mTimeElapsed;
  
  private int[] mWordConfidences;
  private int mMeanConfidence;

  OcrResult(Uri bitmapUri, String text, long timeElapsed) {
    mBitmapUri = bitmapUri;
    mText = text;
    mTimeElapsed = timeElapsed;
  }
  
  OcrResult(Uri bitmapUri, String text, long timeElapsed, int[] wordConfidences,
      int meanConfidence) {
    this(bitmapUri, text, timeElapsed);
    mWordConfidences = wordConfidences;
    mMeanConfidence = meanConfidence;
  }
  
  public void writeToParcel(Parcel out, int flags) {
    out.writeParcelable(mBitmapUri, PARCELABLE_WRITE_RETURN_VALUE);
    out.writeString(mText);
    out.writeLong(mTimeElapsed);
    out.writeIntArray(mWordConfidences);
    out.writeInt(mMeanConfidence);
  }
  
  private OcrResult(Parcel in) {
    mBitmapUri = in.readParcelable(Uri.class.getClassLoader());
    mText = in.readString();
    mTimeElapsed = in.readInt();
    mWordConfidences = in.createIntArray();
    mMeanConfidence = in.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<OcrResult> CREATOR = new Parcelable
      .Creator<OcrResult>() {
    public OcrResult createFromParcel(Parcel in) {
      return new OcrResult(in);
    }
    public OcrResult[] newArray(int size) {
      return new OcrResult[size];
    }
  };
  
  @Override
  public String toString() {
    return mText + " " + " Time elapsed: " + mTimeElapsed;
  }
  
  // Setters and getters
  
  public void setText(String text) {
    mText = text;
  }

  public void setBitmapUri(Uri uri) {
    mBitmapUri = uri;
  }

  public String getText() {
    return mText;
  }
  
  public Uri getBitmapUri() {
    return mBitmapUri;
  }
  
  public void setTimeElapsed(long time) {
    mTimeElapsed = time;
  }
  
  public long getTimeElapsed() {
    return mTimeElapsed;
  }

  public int[] getWordConfidences() {
    return mWordConfidences;
  }
  
  public int getMeanConfidence() {
    return mMeanConfidence;
  }

  public void setWordConfidences(int[] wordConfidences) {
    mWordConfidences = wordConfidences;
  }

  public void setMeanConfidence(int meanConfidence) {
    mMeanConfidence = meanConfidence;
  }
}

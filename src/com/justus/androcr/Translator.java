package com.justus.androcr;

import android.util.Log;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class Translator {
  private static final String TAG = Translator.class.getSimpleName();
  
  /** API credentials */
  private static final String CLIENT_ID = "[YOUR CLIENT ID]";
  private static final String CLIENT_SECRET = "[YOUR CLIENT SECRET]";
  
  /**
   * Translate given string using the Bing Translate API
   * @param text the text to translate
   * @param sourceLangCode code of the source language
   * @param destLangCode code of the target language
   * @return
   */
  public static String translate(String text, String sourceLangCode,
      String targetLangCode) {
    Translate.setClientId(CLIENT_ID);
    Translate.setClientSecret(CLIENT_SECRET);
    Log.d(TAG, "Translating " + sourceLangCode + " -> " + targetLangCode);
    try {
      return Translate.execute(text, Language.fromString(sourceLangCode), 
          Language.fromString(targetLangCode));

    } catch (Exception e) {
      Log.e(TAG, "Translation failed: " + e.getMessage());
      e.printStackTrace();
      return "[Translation unavailable]";
    }
  }
}

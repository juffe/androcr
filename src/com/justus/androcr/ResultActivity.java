package com.justus.androcr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class ResultActivity extends Activity implements OnClickListener {
  private static final String TAG = MainActivity.class.getSimpleName();
  
  private OcrResult mResult;
  private TextView mResultTextView;
  private Button mTranslateBtn;
  private String mTranslatedText;
  private String mSourceLanguageCode;
  private Spinner mTargetLanguageMenu;
  private ProgressDialog mTranslateProgress;
  
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.result);
    Intent intent = getIntent();
    mResult = (OcrResult) intent.getParcelableExtra("ocr_result");
    mSourceLanguageCode = LanguageHelper.mapLanguageCode(
        intent.getStringExtra("language_code"));
    mResultTextView = (TextView) findViewById(R.id.result_text);
    mResultTextView.setText("Original:\n\n" + mResult.getText());
    mTargetLanguageMenu = (Spinner) findViewById(R.id.choose_target_lang);
    mTranslateBtn = (Button) findViewById(R.id.translate_btn);
    mTranslateBtn.setOnClickListener(this);
    populateLanguageMenu();
  }
  
  /** Populate the language spinner with available translation languages */
  private void populateLanguageMenu() {
    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
        R.array.languagenames_microsoft,
        android.R.layout.simple_spinner_dropdown_item);
    mTargetLanguageMenu.setAdapter(adapter);
  }
  
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.translate_btn:
        String targetLanguageName = mTargetLanguageMenu.getSelectedItem().toString();
        String translateMessage = String.format(getString(R.string.translate_progress),
            mSourceLanguageCode, targetLanguageName);
        mTranslateProgress = ProgressDialog.show(this,
            getString(R.string.please_wait), translateMessage, true, false);
        new TranslateAsyncTask().execute(mResult.getText(), mSourceLanguageCode,
            LanguageHelper.getTranslationLanguageCode(this, targetLanguageName));
    }
  }
  
  final private class TranslateAsyncTask extends AsyncTask<String, Void, String> {    
    protected String  doInBackground(String... args) {
      return Translator.translate(args[0], args[1], args[2]);
    }
    
    @Override
    protected void onPostExecute(String translatedText) {
      mTranslateProgress.dismiss();
      mTranslatedText = translatedText;
      mResultTextView.setText("Translated:\n\n" + mTranslatedText);
    }
  }
  
  @Override
  public void onConfigurationChanged(Configuration conf){
    super.onConfigurationChanged(conf);
  }
}

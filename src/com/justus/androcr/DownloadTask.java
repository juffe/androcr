package com.justus.androcr;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.xeustechnologies.jtar.TarEntry;
import org.xeustechnologies.jtar.TarInputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Handles HTTP file downloads and uncompressing
 * 
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class DownloadTask extends AsyncTask<Void, String, Boolean> {
  private static final String TAG = DownloadTask.class.getSimpleName();
  private DownloadCallback mCallback;
  private URL mUrl;
  private File mDestFile;
  private ProgressDialog mProgress;
  
  DownloadTask(DownloadCallback callback, URL url, File dest) {
    mCallback = callback;
    mUrl = url;
    mDestFile = dest;
  }
  
  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    mProgress = new ProgressDialog((Context) mCallback);
    mProgress.setMessage("Downloading file..");
    mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    mProgress.setCancelable(false);
    mProgress.show();
  }
  
  /**
   * Download the file, uncompress it and delete the compressed archive afterwards
   */
  protected Boolean doInBackground(Void... args) {
    try {
      File file = downloadFileHttp(mUrl, mDestFile);
      File uncompressed = gunzip(file, new File(file.toString().replace(".gz", "")));
      Log.d(TAG, "uncompressed: " + uncompressed.getParentFile());
      untar(uncompressed, uncompressed.getParentFile());
      return true;
    } catch (IOException e) {
      Log.e(TAG, "Download failed: " + e.getMessage());
      return false;
    }
  }

  /**
   * Make a HTTP GET request to the given URL and save the response body to
   * a file
   * @param url
   * @param dest
   * @return the resulting file
   * @throws IOException
   */
  private File downloadFileHttp(URL url, File dest) throws IOException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setInstanceFollowRedirects(true);
    conn.setRequestMethod("GET");
    conn.connect();
    if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
      Log.e(TAG, "HTTP request failed");
      Log.e(TAG, "Status code: "  + conn.getResponseCode());
      return null;
    }
    final int BUFFER = 8192;
    int fileSize = conn.getContentLength();
    InputStream input = conn.getInputStream();
    File tempFile = new File(dest.toString() + ".download");
    OutputStream output = new FileOutputStream(tempFile);
    byte[] buf = new byte[BUFFER];
    int bytesRead;
    int downloaded = 0;
    int percentDownloaded = 0;
    while ((bytesRead = input.read(buf, 0, BUFFER)) != -1) {
      output.write(buf, 0, bytesRead);
      downloaded += bytesRead;
      percentDownloaded = (int) ((downloaded / (float) fileSize) * 100);
      publishProgress(String.valueOf(percentDownloaded));
    }
    output.close();
    conn.disconnect();

    File completed = new File(tempFile.toString().replace(".download", ""));
    tempFile.renameTo(completed);
    return completed;
  }
  
  /**
   * Gunzip the given gzipped file
   * @param inputFile the compressed file
   * @param outputFile the destination file
   * @return the uncompressed file
   */
  private File gunzip(File compressedFile, File outputFile) throws IOException {
    final int BUFFER = 8192;
    FileInputStream fis = new FileInputStream(compressedFile);
    GZIPInputStream gzis = new GZIPInputStream(fis, BUFFER);
    FileOutputStream fos = new FileOutputStream(outputFile);
    BufferedOutputStream out = new BufferedOutputStream(fos);
    byte[] buffer = new byte[BUFFER];
    int bytesRead;
    while ((bytesRead = gzis.read(buffer, 0 , BUFFER)) != -1) {
      fos.write(buffer, 0, bytesRead);
    }
    gzis.close();
    out.flush();
    out.close();
    compressedFile.delete();
    return outputFile;
  }
  
  /**
   * Untar a tar archive into the given directory ignoring the relative path.
   * Delete tar file afterwards.
   * @param archive
   * @param destDir
   * @throws IOException
   */
  private void untar(File archive, File destDir) throws IOException {
    TarInputStream tis  = new TarInputStream(new BufferedInputStream(
        new FileInputStream(archive)));
    TarEntry entry;
    while ((entry = tis.getNextEntry()) != null) {
      String pathName = entry.getName();
      String fileName = pathName.substring(pathName.lastIndexOf('/'), pathName.length());     
      FileOutputStream fos = new FileOutputStream(destDir + "/" + fileName);
      BufferedOutputStream out = new BufferedOutputStream(fos);
      final int BUFFER = 8192;
      byte[] buffer = new byte[BUFFER];
      int bytesRead;
      while ((bytesRead = tis.read(buffer, 0, BUFFER)) != -1) {
        out.write(buffer, 0, bytesRead);
      }
      out.flush();
      out.close();
      archive.delete();
    }
    tis.close();
  }
  
  @Override
  protected void onProgressUpdate(String... progress) {
    mProgress.setProgress(Integer.parseInt(progress[0]));
  }
  
  @Override
  protected void onPostExecute(Boolean success) {
    mProgress.dismiss();
    mCallback.onDownloadComplete();
  }
}

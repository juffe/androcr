package com.justus.androcr;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class MainActivity extends Activity implements OcrCallback,
    DownloadCallback, OnClickListener {
  private static final String TAG = MainActivity.class.getSimpleName();
  
  /** Name of directory in which to save application data */
  private static final String STORAGE_DIRNAME = "androcr";
  
  private static final int CAPTURE_IMAGE_REQUEST_CODE = 100;
  private static final int PICK_IMAGE_REQUEST_CODE = 200;
  
  private TessBaseAPI mTess;
  private Uri mImageUri;
  private File mAppDataDir;
  private File mTessDataDir;
  private File mTempStorageDir;
  private Button mTakePictureBtn;
  private Button mPickFromGalleryBtn;
  private Spinner mSourceLanguageMenu;
  private ProgressDialog mOcrProgress;
  private String mSourceLanguageCode;
  private String mFileExistsStr;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    mTess = new TessBaseAPI();
    mTakePictureBtn = (Button) findViewById(R.id.take_picture_btn);
    mPickFromGalleryBtn = (Button) findViewById(R.id.choose_from_galley_btn);
    mSourceLanguageMenu = (Spinner) findViewById(R.id.choose_source_lang);
    mTakePictureBtn.setOnClickListener(this);
    mPickFromGalleryBtn.setOnClickListener(this);
    mFileExistsStr = getString(R.string.file_exists);
    setup();
    populateLanguageMenu();
  }
  
  /**
   * Set up the storage directory structure on the external storage
   * 
   * androcr/
   *    tessdata/ (downloaded language files)
   *    media/ (Temporary storage for captured images)
   *  
   * @throws IOException
   */
  private void setup() {
    File storageDir = Environment.getExternalStorageDirectory();
        mAppDataDir = new File(storageDir, STORAGE_DIRNAME);
    if (! mAppDataDir.exists()) {
      mAppDataDir.mkdir();
    }
    mTessDataDir = new File(mAppDataDir, "tessdata");
    if (! mTessDataDir.exists()) {
      mTessDataDir.mkdir();
    } 
    mTempStorageDir = new File(mAppDataDir, "temp");
    if (! mTempStorageDir.exists()) {
        mTempStorageDir.mkdir();
    }
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.download_language_file:
        showLanguageDownloadDialog();
        break;
      case R.id.delete_language_files:
        deleteLanguageFiles();
        break;
    }
    return super.onOptionsItemSelected(item);
  }
  
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.take_picture_btn:
        launchCamera();
        break;
      case R.id.choose_from_galley_btn:
        pickFromGallery();
        break;
    }
  }
  
  /**
   * Populate the source language spinner with a list of languages that  have a
   * language file downloaded
   */
  private void populateLanguageMenu() {
    ArrayList<String> languages = getDownloadedLanguages();
    if (languages.isEmpty()) {
      languages.add(getString(R.string.no_language_files));
    }
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_spinner_dropdown_item, languages);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    mSourceLanguageMenu.setAdapter(adapter);
  }
  
  /**
   * Get a list of language names that are supported by the OCR engine.
   * If a language file already exists for a language, append that information
   * to the language name
   * 
   * @return the list of language names
   */
  private ArrayList<String> getAvailableLanguages() {
    Resources res = getResources();
    String[] languageNames = res.getStringArray(R.array.languagenames);
    ArrayList<String> languages = new ArrayList<String>();
    for (String languageName : languageNames) {
      // Only get languages that are supported by both tesseract and microsoft
      if (Arrays.asList(res.getStringArray(R.array.languagenames_microsoft))
          .contains(languageName)) {
        String languageString = languageName;
        if (languageFileExists(languageName)) {
          languageString += mFileExistsStr; // Append "File exists"
        }
        languages.add(languageString);
      }
    }
    return languages;
  }
  
  /**
   * Get a list of language names that have a language file downloaded
   * @return the list of language names
   */
  private ArrayList<String> getDownloadedLanguages() {
    ArrayList<File> files = new ArrayList<File>(Arrays.asList(
        mTessDataDir.listFiles()));
    ArrayList<String> languageNames = new ArrayList<String>();
    for (File languageFile : files) {
      String fileName = languageFile.getName();
      if (fileName.contains(".traineddata")) {
        String languageCode = fileName.replace(".traineddata", "");
        languageNames.add(LanguageHelper.getLanguageName(this, languageCode));
      }
    }
    return languageNames;
  }
  
  /**
   * Show the list dialog for donwloading language files
   */
  private void showLanguageDownloadDialog() {
    ArrayList<String> availableLanguages = getAvailableLanguages();
    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_1, availableLanguages);
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle(getString(R.string.download_language_file));
    builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int item) {
        String languageName = adapter.getItem(item).toString();
        if (languageName.contains(mFileExistsStr)) {
          languageName = languageName.replace(mFileExistsStr, "");
        }
        String languageCode = LanguageHelper.getLanguageCode(getBaseContext(),
            languageName);
        downloadLanguageFile(languageCode);
      }
    });
    
    AlertDialog alert = builder.create();
    alert.show();
  }
  
  /**
   * Download a language file for the given  language code
   * @param languageCode iso6393 language code
   */
  private void downloadLanguageFile(String languageCode) {
    URL url = LanguageHelper.getDownloadUrl(languageCode);
    File dest = new File(mTessDataDir, LanguageHelper.getFileName(languageCode));
    if (languageFileExists(LanguageHelper.getLanguageName(this, languageCode))) {
      Toast.makeText(this, getString(R.string.file_exists_msg),
          Toast.LENGTH_SHORT).show();
    } else {
      new DownloadTask((DownloadCallback) this, url, dest).execute();
    }
  }
  
  /** Delete all language files */
  private void deleteLanguageFiles() {
    String[] files = mTessDataDir.list();  
    for (int i = 0; i < files.length; i++) {  
      File languageFile = new File(mTessDataDir, files[i]);   
      languageFile.delete();
    }
    populateLanguageMenu();
    Toast.makeText(this, getString(R.string.files_deleted),
        Toast.LENGTH_SHORT).show();
  }
  
  /** Delete image files from the temporary folder */
  private void deleteTempStorage() {
    String[] files = mTempStorageDir.list();  
    for (int i = 0; i < files.length; i++) {  
      File file = new File(mTempStorageDir, files[i]);   
      file.delete();
    }
  }
  
  /**
   * Check if a language file exists for the given language name
   * @param languageName the name of the language
   * @return
   */
  private boolean languageFileExists(String languageName) {
    ArrayList<String> languages = getDownloadedLanguages();
    return languages.contains(languageName);
  }
  
  /**
   * Called upon completing a donwload
   * Repopulate the language menu after new downloads
   */
  public void onDownloadComplete() {
    populateLanguageMenu();
  }
  
  /** Create an intent to launch a camera app */
  private void launchCamera() {
    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    mImageUri = Image.getOutputUri(mTempStorageDir);
    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
    startActivityForResult(intent, CAPTURE_IMAGE_REQUEST_CODE);
  }
  
  /** Create an intent to pick an image from the media gallery */
  private void pickFromGallery() {
    Intent intent = new Intent(Intent.ACTION_PICK);
    intent.setType("image/*");
    startActivityForResult(intent, PICK_IMAGE_REQUEST_CODE);
  }
  
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      Image image = null;
      switch (requestCode) {
        case CAPTURE_IMAGE_REQUEST_CODE:
          //bitmap = getBitmap(mImageUri.getPath());
          image = new Image(mImageUri.getPath());
          break;
        case PICK_IMAGE_REQUEST_CODE:
          mImageUri = data.getData();
          //bitmap = getBitmapFromMediaStore(mImageUri);
          image = Image.fromMediaStore(this, mImageUri);
          break;
      }
      imageToText(image);
    }
  }
  
  /**
   * Inspect the given image and convert it to text
   * @param image
   */
  private void imageToText(final Image image) {
    String languageName = mSourceLanguageMenu.getSelectedItem().toString();
    mSourceLanguageCode = LanguageHelper.getLanguageCode(this, languageName);
    if (mSourceLanguageCode.isEmpty()) {
      Toast.makeText(this, R.string.no_language_files, Toast.LENGTH_SHORT).show();
      return;
    }
    mOcrProgress = ProgressDialog.show(this, getString(R.string.please_wait),
        getString(R.string.ocr_progress), true, false);
    final OcrCallback callback = this;
    new Thread(new Runnable() {
      public void run() {
        mTess.init(mAppDataDir.getPath(), mSourceLanguageCode);
        new OcrTask(mTess, callback, mImageUri).execute(image.getBitmap());
      }
    }).start();
  }
  
  /**
   * Called upon completing an OCR task
   * @param result the result of the OCR task
   */
  public void onOcrComplete(OcrResult result) {
    mOcrProgress.dismiss();
    Intent intent = new Intent(this, ResultActivity.class);
    intent.putExtra("ocr_result", result);
    intent.putExtra("language_code", mSourceLanguageCode);
    deleteTempStorage();
    startActivity(intent);
  }
  
  @Override
  public void onConfigurationChanged(Configuration conf){
    super.onConfigurationChanged(conf);
  }
  
}

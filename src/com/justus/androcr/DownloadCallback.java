package com.justus.androcr;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public interface DownloadCallback {
  
  /**
   * Called upon completing an asynchronous downloada task
   */
  public void onDownloadComplete();
}

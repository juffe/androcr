package com.justus.androcr;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public class Image {
  private static final String TAG = Image.class.getSimpleName();
  
  private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
  private static final int SAMPLE_SIZE = 2;
  private static final int DENSITY = DisplayMetrics.DENSITY_MEDIUM;
  
  protected String mPath;
  
  public Image(String path) {
    mPath = path;
  }
  
  /**
   * Get the bitmap representation of the image and apply rotation if necessary
   * @param path
   * @return the bitmap
   */
  public Bitmap getBitmap() {
    Bitmap bitmap = BitmapFactory.decodeFile(mPath, getBitmapOptions());
    int rotation = getRotation();
    if (rotation != 0) {
      bitmap = rotate(bitmap, rotation);
    }
    return bitmap;
  }
  
  /**
   * Rotate a bitmap the given amount of degrees
   * @param bitmap
   * @param degrees
   * @return the rotated bitmap
   */
  public static Bitmap rotate(Bitmap bitmap, int degrees) {
    int w = bitmap.getWidth();
    int h = bitmap.getHeight();
    Matrix matrix = new Matrix();
    matrix.preRotate(degrees);
    bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, false);
    return bitmap.copy(Bitmap.Config.ARGB_8888, true);
  }
  
  /**
   * Get the rotation of the image in degrees
   * @return the rotation in degrees
   */
  public int getRotation() {
    int rotate = 0;
    try {
      ExifInterface exif = new ExifInterface(mPath);
      int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
          ExifInterface.ORIENTATION_NORMAL);
      switch (orientation) {
        case ExifInterface.ORIENTATION_ROTATE_90:
          rotate = 90;
          break;
        case ExifInterface.ORIENTATION_ROTATE_180:
          rotate = 180;
          break;
        case ExifInterface.ORIENTATION_ROTATE_270:
          rotate = 270;
          break;
      }
    } catch (IOException e) {
      Log.e(TAG, "Failed to load bitmap");
      e.printStackTrace();
    }
    return rotate;
  }
  
  /**
   * Get the global options for BitmapFactory
   * @return the Options object
   */
  private static Options getBitmapOptions() {
    Options opts = new BitmapFactory.Options();
    opts.inPreferredConfig = BITMAP_CONFIG;
    opts.inSampleSize = SAMPLE_SIZE;
    opts.inScreenDensity = DENSITY;
    return opts;
  }
  
  /**
   * Factory method for getting an image from the media storage by its URI
   * @param context
   * @param uri
   * @return the image object
   */
  public static Image fromMediaStore(Context context, Uri uri) {
    String[] filePathColumn = {MediaStore.Images.Media.DATA};
    Cursor cursor = context.getContentResolver().query(uri, filePathColumn,
        null, null, null);
    cursor.moveToFirst();
    int index = cursor.getColumnIndex(filePathColumn[0]);
    String filePath = cursor.getString(index);
    cursor.close();
    return new Image(filePath);
  }
  
  /**
   * Create a file Uri for saving an image
   * @param mediaStorageDir
   */
  public static Uri getOutputUri(File mediaStorageDir) {
    return Uri.fromFile(getOutputFile(mediaStorageDir));
  }

  /**
   * Create a File for saving an image
   * @param mediaStorageDir
   */
  private static File getOutputFile(File mediaStorageDir) {
    String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
        .format(new Date());
    return new File(mediaStorageDir.getPath() + File.separator + "IMG_" +
        timestamp + ".jpg");
  }

}

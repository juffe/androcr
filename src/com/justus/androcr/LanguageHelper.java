package com.justus.androcr;

import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 */
public class LanguageHelper {
  private static final String TAG = LanguageHelper.class.getSimpleName();
  
  private static final String DOWNLOAD_URL = "https://tesseract-ocr.googlecode.com/files/";
  private static final String VERSION = "3.02";
  
  /**
   * Map an ISO 639-3 language code to an ISO 639-1 language code.
   * 
   * @param languageCode ISO 639-3 language code
   * @return ISO 639-1 language code
   */
  public static String mapLanguageCode(String languageCode) {   
    if (languageCode.equals("afr")) { // Afrikaans
      return "af";
    } else if (languageCode.equals("sqi")) { // Albanian
      return "sq";
    } else if (languageCode.equals("ara")) { // Arabic
      return "ar";
    } else if (languageCode.equals("aze")) { // Azeri
      return "az";
    } else if (languageCode.equals("eus")) { // Basque
      return "eu";
    } else if (languageCode.equals("bel")) { // Belarusian
      return "be";
    } else if (languageCode.equals("ben")) { // Bengali
      return "bn";
    } else if (languageCode.equals("bul")) { // Bulgarian
      return "bg";
    } else if (languageCode.equals("cat")) { // Catalan
      return "ca";
    } else if (languageCode.equals("chi_sim")) { // Chinese (Simplified)
      return "zh-CN";
    } else if (languageCode.equals("chi_tra")) { // Chinese (Traditional)
      return "zh-TW";
    } else if (languageCode.equals("hrv")) { // Croatian
      return "hr";
    } else if (languageCode.equals("ces")) { // Czech
      return "cs";
    } else if (languageCode.equals("dan")) { // Danish
      return "da";
    } else if (languageCode.equals("nld")) { // Dutch
      return "nl";
    } else if (languageCode.equals("eng")) { // English
      return "en";
    } else if (languageCode.equals("est")) { // Estonian
      return "et";
    } else if (languageCode.equals("fin")) { // Finnish
      return "fi";
    } else if (languageCode.equals("fra")) { // French
      return "fr";
    } else if (languageCode.equals("glg")) { // Galician
      return "gl";
    } else if (languageCode.equals("deu")) { // German
      return "de";
    } else if (languageCode.equals("ell")) { // Greek
      return "el";
    } else if (languageCode.equals("heb")) { // Hebrew
      return "he";
    } else if (languageCode.equals("hin")) { // Hindi
      return "hi";
    } else if (languageCode.equals("hun")) { // Hungarian
      return "hu";
    } else if (languageCode.equals("isl")) { // Icelandic
      return "is";
    } else if (languageCode.equals("ind")) { // Indonesian
      return "id";
    } else if (languageCode.equals("ita")) { // Italian
      return "it";
    } else if (languageCode.equals("jpn")) { // Japanese
      return "ja";
    } else if (languageCode.equals("kan")) { // Kannada
      return "kn";
    } else if (languageCode.equals("kor")) { // Korean
      return "ko";
    } else if (languageCode.equals("lav")) { // Latvian
      return "lv";
    } else if (languageCode.equals("lit")) { // Lithuanian
      return "lt";
    } else if (languageCode.equals("mkd")) { // Macedonian
      return "mk";
    } else if (languageCode.equals("msa")) { // Malay
      return "ms";
    } else if (languageCode.equals("mal")) { // Malayalam
      return "ml";
    } else if (languageCode.equals("mlt")) { // Maltese
      return "mt";
    } else if (languageCode.equals("nor")) { // Norwegian
      return "no";
    } else if (languageCode.equals("pol")) { // Polish
      return "pl";
    } else if (languageCode.equals("por")) { // Portuguese
      return "pt";
    } else if (languageCode.equals("ron")) { // Romanian
      return "ro";
    } else if (languageCode.equals("rus")) { // Russian
      return "ru";
    } else if (languageCode.equals("srp")) { // Serbian (Latin)
      return "sr";
    } else if (languageCode.equals("slk")) { // Slovak
      return "sk";
    } else if (languageCode.equals("slv")) { // Slovenian
      return "sl";
    } else if (languageCode.equals("spa")) { // Spanish
      return "es";
    } else if (languageCode.equals("swa")) { // Swahili
      return "sw";
    } else if (languageCode.equals("swe")) { // Swedish
      return "sv";
    } else if (languageCode.equals("tgl")) { // Tagalog
      return "tl";
    } else if (languageCode.equals("tam")) { // Tamil
      return "ta";
    } else if (languageCode.equals("tel")) { // Telugu
      return "te";
    } else if (languageCode.equals("tha")) { // Thai
      return "th";
    } else if (languageCode.equals("tur")) { // Turkish
      return "tr";
    } else if (languageCode.equals("ukr")) { // Ukrainian
      return "uk";
    } else if (languageCode.equals("vie")) { // Vietnamese
      return "vi";
    } else {
      return "";
    }
  }
  
  /**
   * Map the given ISO 639-3 language code to a language name
   * 
   * @param context
   * @param languageCode
   * @return name of the language
   */
  public static String getLanguageName(Context context, String languageCode) {
    Resources res = context.getResources();
    String[] languageCodes= res.getStringArray(R.array.iso6393);
    String[] languageNames = res.getStringArray(R.array.languagenames);
    for (int i = 0; i < languageCodes.length; i++) {
      if (languageCodes[i].equals(languageCode)) {
        return languageNames[i];
      }
    }
    Log.d(TAG, "Could not find language name for " + languageCode);
    return "";
  }
  
  /**
   * Map the given language name to ISO 639-3 language code
   * 
   * @param context
   * @param languageName
   * @return the language code
   */
  public static String getLanguageCode(Context context, String languageName) {
    Resources res = context.getResources();
    String[] languageCodes= res.getStringArray(R.array.iso6393);
    String[] languageNames = res.getStringArray(R.array.languagenames);
    for (int i = 0; i < languageNames.length; i++) {
      if (languageNames[i].equals(languageName)) {
        return languageCodes[i];
      }
    }
    Log.d(TAG, "Could not find language code for " + languageName);
    return "";
  }
  /**
   * Map the given language name to ISO 639-1 language code for translating
   * 
   * @param context
   * @param languageName
   * @return the language code
   */
  public static String getTranslationLanguageCode(Context context, String languageName) {
    return LanguageHelper.mapLanguageCode(LanguageHelper.getLanguageCode(context, languageName));
  }
  
  /**
   * Determine if the given string is a valid language name
   * 
   * @param context
   * @param string the string to check
   * @return boolean
   */
  public static boolean isLanguageName(Context context, String string) {
    Resources res = context.getResources();
    String[] languageNames = res.getStringArray(R.array.languagenames);
    for (int i = 0; i < languageNames.length; i++) {
      if (string.equals(languageNames[i])) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Get the language file download URL fot the given language code
   * @param langCode
   * @return the URL 
   */
  public static URL getDownloadUrl(String langCode) {
    URL url = null;
    try {
      url = new URL(DOWNLOAD_URL + getFileName(langCode));  
    } catch(MalformedURLException e) {
      Log.e(TAG, e.getMessage());
    }
    return url;
  }
  
  /**
   * Get the file name of the the language data for given language code
   * @param langCode
   * @return
   */
  public static String getFileName(String langCode) {
    return "tesseract-ocr-" + VERSION + "." + langCode + ".tar.gz";
  }

}

package com.justus.androcr;

/**
 * @author Justus Jylhä <justus.jylha@metropolia.fi>
 * @package com.justus.androcr
 */
public interface OcrCallback {
  
  /**
   * Called upon completing an OCR scan
   * @param result the returned OcrResult object
   */
  public void onOcrComplete(OcrResult result);
}
